import { Component, OnInit } from '@angular/core';
import { NavService } from 'src/app/nav/nav.service';

@Component({
  selector: 'zen-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  constructor(private readonly navService: NavService) {}

  ngOnInit(): void {
    this.navService.setBreadcrumb({
      title: 'Welcome to Zen Linux',
      link: '/',
    });
  }
}
