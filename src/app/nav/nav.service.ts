import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

export interface Breadcrumb {
  title: string;
  link: string;
}

@Injectable({
  providedIn: 'root',
})
export class NavService {
  breadcrumbs: Observable<Breadcrumb[]>;

  private breadcrumbsSubject: BehaviorSubject<Breadcrumb[]>;

  constructor() {
    this.breadcrumbsSubject = new BehaviorSubject<Breadcrumb[]>([]);
    this.breadcrumbs = this.breadcrumbsSubject.asObservable();
  }

  setBreadcrumb(breadcrumb: Breadcrumb) {
    this.breadcrumbsSubject.next([breadcrumb]);
  }
}
