import { Component, OnInit } from '@angular/core';
import { ApolloError } from '@apollo/client/errors';
import { NavService } from 'src/app/nav/nav.service';
import { AuthService } from '../auth.service';

@Component({
  selector: 'zen-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  email = '';
  username = '';
  password = '';
  error?: string;

  constructor(
    private readonly navService: NavService,
    private readonly authService: AuthService
  ) {}

  ngOnInit(): void {
    this.navService.setBreadcrumb({ title: 'Login', link: '/login' });
  }

  onLogin() {
    console.log('login...');
    this.error = undefined;
    this.authService.signup(this.email, this.password).subscribe(
      (response) => {
        console.log('got signup response:', response);
      },
      (error) => {
        if (error instanceof ApolloError) {
          this.error = error.message;
        } else {
          throw error;
        }
      }
    );
  }
}
