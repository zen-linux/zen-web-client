import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RepositoriesComponent } from './repositories/repositories.component';

@NgModule({
  imports: [CommonModule],
  declarations: [RepositoriesComponent],
})
export class RepositoryModule {}
