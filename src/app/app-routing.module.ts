import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './auth/login/login.component';
import { SignupComponent } from './auth/signup/signup.component';
import { AboutComponent } from './content/about/about.component';
import { NotFoundComponent } from './content/not-found/not-found.component';
import { DownloadsComponent } from './download/downloads/downloads.component';
import { HomeComponent } from './home/home/home.component';
import { PackagesComponent } from './package/packages/packages.component';
import { RepositoriesComponent } from './repository/repositories/repositories.component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
  },
  {
    path: 'downloads',
    component: DownloadsComponent,
  },
  {
    path: 'packages',
    component: PackagesComponent,
  },
  {
    path: 'repositories',
    component: RepositoriesComponent,
  },
  {
    path: 'about',
    component: AboutComponent,
  },
  {
    path: 'login',
    component: LoginComponent,
  },
  {
    path: 'register',
    component: SignupComponent,
  },
  {
    path: '**',
    component: NotFoundComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
