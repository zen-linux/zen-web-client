import { Component, OnInit } from '@angular/core';
import { NavService } from 'src/app/nav/nav.service';

@Component({
  selector: 'zen-repositories',
  templateUrl: './repositories.component.html',
  styleUrls: ['./repositories.component.scss'],
})
export class RepositoriesComponent implements OnInit {
  constructor(private readonly navService: NavService) {}
  ngOnInit(): void {
    this.navService.setBreadcrumb({
      title: 'Official Zen Linux Repositories',
      link: '/repositories',
    });
  }
}
