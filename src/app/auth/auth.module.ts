import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { SignupComponent } from './signup/signup.component';

@NgModule({
  imports: [CommonModule, FormsModule, RouterModule],
  declarations: [LoginComponent, SignupComponent],
  exports: [LoginComponent, SignupComponent],
})
export class AuthModule {}
