import { Component, OnInit } from '@angular/core';
import { NavService } from 'src/app/nav/nav.service';

@Component({
  selector: 'zen-downloads',
  templateUrl: './downloads.component.html',
  styleUrls: ['./downloads.component.scss'],
})
export class DownloadsComponent implements OnInit {
  constructor(private readonly navService: NavService) {}
  ngOnInit(): void {
    this.navService.setBreadcrumb({
      title: 'Download ISO files and more',
      link: '/downloads',
    });
  }
}
