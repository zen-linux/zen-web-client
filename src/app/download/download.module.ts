import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { DownloadsComponent } from './downloads/downloads.component';

@NgModule({
  imports: [CommonModule],
  declarations: [DownloadsComponent],
  exports: [DownloadsComponent],
})
export class DownloadModule {}
