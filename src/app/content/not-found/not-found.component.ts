import { Component, OnInit } from '@angular/core';
import { NavService } from 'src/app/nav/nav.service';

@Component({
  selector: 'zen-not-found',
  templateUrl: './not-found.component.html',
  styleUrls: ['./not-found.component.scss'],
})
export class NotFoundComponent implements OnInit {
  constructor(private readonly navService: NavService) {}
  ngOnInit(): void {
    this.navService.setBreadcrumb({ title: '404', link: '/404' });
  }
}
