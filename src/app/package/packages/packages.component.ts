import { Component, OnInit } from '@angular/core';
import { NavService } from 'src/app/nav/nav.service';

@Component({
  selector: 'zen-packages',
  templateUrl: './packages.component.html',
  styleUrls: ['./packages.component.scss'],
})
export class PackagesComponent implements OnInit {
  constructor(private readonly navService: NavService) {}
  ngOnInit(): void {
    this.navService.setBreadcrumb({
      title: 'Pacman Packages',
      link: '/packages',
    });
  }
}
