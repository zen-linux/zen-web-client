import { Injectable } from '@angular/core';
import { Apollo, gql } from 'apollo-angular';
import { Observable, map, tap } from 'rxjs';
import { SignupResponseDto } from '../dto/signup-response.dto';

const SIGNUP_MUTATION = gql`
  mutation Signup($email: String!, $password: String!) {
    signup(signupInput: { email: $email, password: $password }) {
      message
    }
  }
`;

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  constructor(private readonly apollo: Apollo) {}

  signup(email: string, password: string): Observable<SignupResponseDto> {
    return this.apollo
      .mutate<SignupResponseDto>({
        mutation: SIGNUP_MUTATION,
        variables: {
          email,
          password,
        },
      })
      .pipe(
        tap((result) => console.log('signup result:', result)),
        map((result) => result.data || { message: 'test' })
      );
  }
}
