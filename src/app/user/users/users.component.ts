import { Component, OnInit } from '@angular/core';
import { Apollo, gql } from 'apollo-angular';
import { Observable, map } from 'rxjs';

@Component({
  selector: 'zen-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss'],
})
export class UsersComponent implements OnInit {
  users$: Observable<any[]>;
  loading = true;
  error: any;

  constructor(private readonly apollo: Apollo) {
    this.users$ = this.apollo
      .watchQuery<any>({
        query: gql`
          query {
            users {
              id
              email
              name
            }
          }
        `,
      })
      .valueChanges.pipe(map((result) => result.data.users));
  }

  ngOnInit(): void {
    console.log('bla');
  }
}
