import { Component, OnInit } from '@angular/core';
import { NavService } from 'src/app/nav/nav.service';

@Component({
  selector: 'zen-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss'],
})
export class AboutComponent implements OnInit {
  constructor(private readonly navService: NavService) {}

  ngOnInit(): void {
    this.navService.setBreadcrumb({ title: 'About Zen Linux', link: '/about' });
  }
}
