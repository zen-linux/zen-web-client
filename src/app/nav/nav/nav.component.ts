import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { Breadcrumb, NavService } from '../nav.service';

@Component({
  selector: 'zen-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss'],
})
export class NavComponent {
  readonly breadcrumbs: Observable<Breadcrumb[]>;

  constructor(readonly navService: NavService) {
    this.breadcrumbs = navService.breadcrumbs;
  }
}
