import { Component, OnInit } from '@angular/core';
import { ApolloError } from '@apollo/client/errors';
import { NavService } from 'src/app/nav/nav.service';
import { AuthService } from '../auth.service';

@Component({
  selector: 'zen-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss'],
})
export class SignupComponent implements OnInit {
  email = '';
  password = '';
  error?: string;

  constructor(
    private readonly authService: AuthService,
    private readonly navService: NavService
  ) {}

  ngOnInit(): void {
    this.navService.setBreadcrumb({
      title: 'Create a new account',
      link: '/register',
    });
  }

  onSignup() {
    console.log('login...');
    this.error = undefined;
    this.authService.signup(this.email, this.password).subscribe(
      (response) => {
        console.log('got signup response:', response);
      },
      (error) => {
        if (error instanceof ApolloError) {
          this.error = error.message;
        } else {
          throw error;
        }
      }
    );
  }
}
