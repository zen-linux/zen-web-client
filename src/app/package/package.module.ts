import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { PackagesComponent } from './packages/packages.component';

@NgModule({
  imports: [CommonModule],
  declarations: [PackagesComponent],
})
export class PackageModule {}
